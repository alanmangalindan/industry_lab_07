package ictgradschool.industry.exceptions.simpleexceptions;

import java.util.Scanner;


public class SimpleExceptions {
    public static void main(String[] args) {
        SimpleExceptions exceptions = new SimpleExceptions();

        //Question 1 & 2
//        exceptions.handlingException();


        //Question3
//        exceptions.Question3();

        //Question4
        exceptions.Question4();
    }

    /**
     * The following tries to divide using two user input numbers, but is
     * prone to error.
     */
    public void handlingException() {
        Scanner sc = new Scanner(System.in);

        try {
        System.out.print("Enter the first number: ");
        String str1 = sc.next();
        int num1 = Integer.parseInt(str1);
        System.out.print("Enter the second number: ");
        String str2 = sc.next();
        int num2 = Integer.parseInt(str2);

        // Output the result
        System.out.println("The division of " + num1 + " over " + num2 + " is " + (num1 / num2) + "\n");
        } catch (ArithmeticException e) {
            System.out.println("Division by 0 is not possible.");
        } catch (NumberFormatException e) {
            System.out.println("Please enter a valid integer.");
        }

    }

    public void Question3() {
        //Write some Java code which throws a StringIndexOutOfBoundsException
        String s = "hello";
        char c = s.charAt(s.length()); // throws StringIndexOutOfBoundsException
    }

    public void Question4() {
        //Write some Java code which throws a ArrayIndexOutOfBoundsException
        int[] intArray = new int[5];
        int num = intArray[5]; // throws ArrayIndexOutOfBoundsException
    }
}