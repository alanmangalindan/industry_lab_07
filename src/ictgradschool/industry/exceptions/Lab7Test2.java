package ictgradschool.industry.exceptions;

public class Lab7Test2 {

    public void start() {
        processA("0");
    }

    private void processA(String input) {
        int num = 0;
        try {
            num = Integer.parseInt(input);
            num = 100 / num;
            System.out.println( "A" );
        } catch( ArithmeticException e ) {
            System.out.println( "B Error" );
        } catch( NumberFormatException e ) {
            System.out.println( "C Error" );
        } finally {
            System.out.println( "D" );
        }
        System.out.println( "E" );
    }
    public static void main(String[] args) {
        Lab7Test2 p = new Lab7Test2();
        p.start();
    }
}
