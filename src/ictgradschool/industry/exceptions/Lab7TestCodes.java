package ictgradschool.industry.exceptions;

import ictgradschool.Keyboard;

public class Lab7TestCodes {

    public static void main(String[] args) {
        Lab7TestCodes test = new Lab7TestCodes();
//        test.start();
//        test.tryCatch01();
//        test.tryCatch02();
//        test.tryCatch03();
//        test.tryCatch06();
//        System.out.println("result is: " + test.getUserInt("Enter an Integer"));
//        test.throwsClause09();
//        test.enterPassword();
//        test.tryCatch07();
//        test.tryCatch08();
//        test.tryCatch08a();
//        try {
            test.throwsClause10();
//        } catch (NullPointerException e) {
//            System.out.println("String numS is null! Exception handled.");
//        }

    }


    public void start() {

        int num1 = 10 - 5 * 5 + 4;
        int num2 = 10 - 5 * (5 + 4);
        int num3 = 10 + 5 * (5 - 4);
        int num4 = num1 + num2 + num3;
        System.out.println(num3 + " " + num4);

    }

    private void tryCatch01() {
        int result = 0;
        int[] nums = null;
        try {
            result = nums.length;
            System.out.println("See you");
        } catch (ArithmeticException e) {
            System.out.println("Problem");
            result = -1;
        }
        System.out.println("Result: " + result);
    }

    private void tryCatch02() {
        int num1 = 120, num2 = 120, result = 0;
        try {
            result = num2 / (num1 - num2);
            System.out.println("Result: " + result);
        } catch (ArithmeticException e) {
            System.out.println("Invalid result: Division by zero");
            System.out.println(e.getMessage());
        }
    }

    private void tryCatch03() {
        int result = 0;
        String[] items = {"one", "two", null};
        try{
            result = items[2].length();
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("Result: " + result);
    }

    private void tryCatch04() {
        int num = 0;
        try {
            System.out.println("Enter number: ");
            num = Integer.parseInt(Keyboard.readInput());
            System.out.println("Thank you");
        } catch (NumberFormatException e) {
            System.out.println("Input error");
            num = -1;
        }
        System.out.println("Number: " + num);
    }

    private void tryCatch06() {
        try {
            try06(0, "ABC");
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println("B Error");
        }
    }
    private void try06(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            num = 200 / num;
        } catch (NullPointerException e) {
            System.out.println("E error");
        }
        System.out.println("F");
    }

    private void tryCatch07() {
        try07(0, "hello");
        System.out.println("A");
    }
    private void try07(int num, String s) {
        System.out.println("B");
        try {
            num = s.length();
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("C");
    }

    private void tryCatch08() {
        try {
            try08(0, null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println("B");
        }
    }
    private void try08(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            System.out.println("D");
        } finally {
            System.out.println("E");
        }
        System.out.println("F");
    }


    private void tryCatch08a() {
        try08a(0, "hello");
        System.out.println("A");
    }
    private void try08a(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            System.out.println("D");
        }catch (NullPointerException e) {
            System.out.println("B");
        } finally {
            System.out.println("E");
        }
        System.out.println("F");
    }

    private int getUserInt(String prompt) {
        while (true) {
            System.out.print(prompt + ": ");
            String userStr = Keyboard.readInput();
            try {
                int userInt = Integer.parseInt(userStr);
                return userInt;
            } catch (NumberFormatException e) {
                System.out.println("Please enter a valid int");
            }
        }

    }

//    private int getUserInt(String prompt) {
//        int userInt = 0;
////        while (true) {
//        System.out.print(prompt + ": ");
//        String userStr = Keyboard.readInput();
//        try {
//            userInt = Integer.parseInt(userStr);
//        } catch (NumberFormatException e) {
//            System.out.println("Invalid Input");
//            getUserInt("Enter an Integer");
//        }
//        return userInt;
////        }
//
//    }

    private void throwsClause10() {
        try {
            throws10(null);
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }
    private void throws10(String numS) {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }

    private void throwsClause09() {
        try {
            throws09(null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        System.out.println("B");
    }
    private void throws09(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Null String");
        }
        System.out.println("C");
    }

    public void validatePassword (String password) throws InvalidPasswordException {

        if (!password.equals("pa55w0rD")) {
            throw new InvalidPasswordException("Incorrect password");
        }

    }

    public void enterPassword() {
        while (true) {
            System.out.print("Enter your password: ");
            String password = Keyboard.readInput();

            try {
                validatePassword(password);
                System.out.println("The password is correct!");
                System.out.println("Goodbye!");
                System.exit(0);

            } catch (InvalidPasswordException e) {
                System.out.println(e.getMessage());
            }
        }


    }

}
