package ictgradschool.industry.exceptions.customexceptions;

import ictgradschool.Keyboard;

public class CustomExceptions {

    public void start() {

        String words;
        while (true) {
            try {
                words = getUserInput();
                break;
            } catch (ExceedMaxStringLengthException e) {
                System.out.println(e.getMessage());
                System.out.println("Please enter a string again.");
            }
        }

        String[] wordsSplit = words.split(" ");

        char[] result = getResult(wordsSplit);

        try {
            printResult(result);
        } catch (InvalidWordException e) {
            System.out.println(e.getMessage());
        }

    }

    public String getUserInput() throws ExceedMaxStringLengthException {
        System.out.print("Enter a string of at most 100 characters: ");
        String userInput = Keyboard.readInput();
        if (userInput.length() > 100) {
            throw new ExceedMaxStringLengthException("100 character limit exceeded.");
        } else {
            return userInput;
        }
    }

    public char[] getResult(String[] wordsSplit) {

        char[] result = new char[wordsSplit.length];
        int index = 0;
        for (String s: wordsSplit) {
            if (!s.equals("")) {
                result[index] = s.toUpperCase().charAt(0);
                index++;
            }
        }

        return result;

    }

    public void printResult (char[] result) throws InvalidWordException {

        System.out.print("You entered: ");

        for (int i = 0; i < result.length; i++) {
            try {
                int num = Integer.parseInt(result[i] + "");
                System.out.println();
                throw new InvalidWordException("Invalid word found. Word begins with a number. Stopping the program...");
            } catch (NumberFormatException e) {
                if (result[i] != '0') {
                    System.out.print(result[i] + " ");
                }
            }
        }

        System.out.println();
    }

    public static void main(String[] args) {
        new CustomExceptions().start();
    }
}
